import React, {Component} from 'react';
import Navbar from "./layout/Navbar";
import Users from "./components/Users";
import AddUser from "./form/AddUser";
import UpdateUser from "./form/UpdateUser";
import NotFound from "./pages/NotFound";
import Contribute from "./pages/Contribute";
import {BrowserRouter as Router,Route,Switch} from "react-router-dom";
// import Test from "./components/Test";
import './App.css';


class App extends Component {
            
  render() {
    return (
      <Router>
          <div className="container">
            {/* <Test test = "yoxlamaq" />  */}
            <Navbar title="User App"/>
            <hr /> 
            <Switch>
            <Route exact path = "/" component = {Users} />
            <Route exact path = "/adduser" component = {AddUser} />
            <Route exact path = "/git" component = {Contribute} />
            <Route exact path = "/edit/:id" component = {UpdateUser} />
            <Route component = {NotFound} />
            </Switch>
          </div>
      </Router>
    );
  }
}

export default App;
