import React from 'react'

function Contribute() {
    return (
        <div>
            <h3>Project Files And Contribute to Project</h3>
            <p>You can download full project from <a href="https://gitlab.com/valehhuseynov/reactjs">this</a> github page 
            Also, you can contribute to this project</p>
        </div>
    )
}

export default Contribute;