import React, { Component } from 'react'
import PropTypes from 'prop-types'
import UserConsumer from "../context";
import axios from "axios";
import {Link} from "react-router-dom";

class User extends Component {
    state = {
        isVisiable : false
    }
    static defaultProps = {
        name : "Melumat Yoxdur",
        salary : "Melumat Yoxdur",
        department : "Melumat Yoxdur"
    }
    // constructor(props) {
    //     super(props);

    //     this.state = {
    //         isVisiable : false
    //     }
    // }
    // constructor(props) {
    //     super(props)
    //     this.onClickEvent = this.onClickEvent.bind(this);
    // }


    onClickEvent = (e) => {
        this.setState({
            isVisiable : !this.state.isVisiable
        })
    }

    onDeleteUser = async (dispatch , e) => {
        const {id} = this.props;
        // Delete Request
        await axios.delete(`http://localhost:3004/users/${id}`);
        dispatch({type : "DELETE_USER" , payload : id});
    }

    render() {
        // Destructing
        const {id,name,department,salary} = this.props;
        const {isVisiable} = this.state;

        return (
            <UserConsumer>
                {
                    value => {
                        const {dispatch} = value;
                        return (
                            <div className="col-md-8 mb-4">
                               <div className="card" style = {isVisiable ? {backgroundColor : "#0fc9d8" , color : "white"} : null}>
                                   <div className="card-header d-flex justify-content-between align-items-center">
                                       <h4 className="d-inline" onClick = {this.onClickEvent}>{name}</h4>
                                       <i onClick={this.onDeleteUser.bind(this,dispatch)} className="far fa-trash-alt" style={{cursor: "grab", color: "yellow"}}></i>
                                   </div>
                                   {
                                       isVisiable ? 
                                        <div className="card-body">
                                            <p className="card-text">Salary: {salary}</p>
                                            <p className="card-text">Department: {department}</p>
                                            <Link to = {`edit/${id}`} className="btn btn-dark btn-block">Update User</Link>
                                        </div> : null
                                   }
                               </div>
                            </div>
                        )
                    }
                }
            </UserConsumer>
        )
        
    }
}

User.propTypes = {
    name : PropTypes.string.isRequired,
    salary : PropTypes.string.isRequired,
    department : PropTypes.string.isRequired,
    id : PropTypes.string.isRequired
}



export default User;